import http from 'http';
import express, { Express } from 'express';
import { Request, Response, NextFunction, Router } from 'express';
import { Client } from 'ts-postgres';

interface Point {
  x: Number,
  y: Number,
  z: Number
}

interface AudioMedia {
  object_id: Number;
  media_type: String;
  url: String;
  user_id: Number;
  location1: Point;
  volume: Number;
}

interface VisualMedia {
  media_type: String;
  object_id: Number;
  url: String;
  user_id: Number;
  location1: Point;
  location2: Point;
  location3: Point;
}

//get all inventory (media not necessarily placed in the virtual world yet)
const getAllInventory = async (req: Request, res: Response, next: NextFunction) => {
    const allInventory = []; 
    const client = await getDB();
    const result = client.query(
      `SELECT audio_id as id, url FROM audio_media  
      UNION
      SELECT id_2d, url FROM media_2d
      UNION
      SELECT id_3d, url FROM media_3d;`
    );
      for await (const row of result) {
        const inventory = {
          id: row.get('id'),
          url: row.get('url')
        }
        allInventory.push(inventory); }
        
        return res.status(200).json({
            message: allInventory
        });
}

// get all media visible in the virtual world
const getAllMedia = async (req: Request, res: Response, next: NextFunction) => {
    const allMedia = []; 
    const client = await getDB();
    // get all audio media
    const result_audio = client.query(
      `SELECT uam.id, url, user_id, locationx, locationy, locationz, volume 
      FROM audio_media AS am
      JOIN user_audiomedia AS uam 
      ON 
      uam.audio_id = am.audio_id;`      
    );
    for await (const row of result_audio) {
      const audiomedia = { 
      object_id: row.get('id'), 
      media_type: 'audio',
      url: row.get('url'),
      user_id: row.get('user_id'),
      location1: {x: row.get('locationx'), y: row.get('locationy'), z: row.get('locationz')},
      volume: row.get('volume')
    } as AudioMedia;
      allMedia.push(audiomedia);
    }
      //get all VisualMedia, 2d and 3d
        const result_visual = client.query(
       `SELECT u2d.id as id, url, user_id, locationx_tr as x1, locationy_tr as y1, locationz_tr as z1, locationx_bl as x2, locationy_bl as y2, locationz_bl as z2,
       locationx_br as x3, locationy_br as y3, locationz_br as z3, 
       '2d' as media_type FROM media_2d as m2d
       JOIN 
       user_2dmedia as u2d
       ON 
       m2d.id_2d = u2d.id_2d
       UNION
       SELECT u3d.id, url, user_id, locationx_ftr, locationy_ftr, locationz_ftr, locationx_fbl, locationy_fbl, locationz_fbl,locationx_bbr, locationy_bbr, locationz_bbr, '3d' as media_type FROM media_3d as m3d 
       JOIN
       user_3dmedia as u3d
       ON 
       m3d.id_3d = u3d.id_3d;`
     ); 
       for await (const row of result_visual) {
         const visualmedia = {
          media_type: row.get('media_type'),
          object_id: row.get('id'),
          url: row.get('url'),
          user_id: row.get('user_id'),
          location1: {x: row.get('x1'), y: row.get('y1'), z: row.get('z1')},
          location2: {x: row.get('x2'), y: row.get('y2'), z: row.get('z2')},
          location3: {x: row.get('x3'), y: row.get('y3'), z: row.get('z3')}     
        } as VisualMedia;
        allMedia.push(visualmedia);
       }
    return res.status(200).json({
        message: allMedia
    });
};

// note: media_id is from media inventory, object_id refers to the unique object in space
// duplicate media. places an object from inventory into the virtual world
const addMedia = async (req: Request, res: Response, next: NextFunction) => {
    const client = await getDB();
    if (req.body.media_type == 'audio') {
         await client.query(
        `INSERT INTO user_audiomedia (audio_id, user_id, locationx, locationy, locationz, volume)
        VALUES
        ($1, $2, $3, $4, $5, $6)`,
        [req.body.media_id, req.body.user_id, req.body.location1.x, req.body.location1.y, req.body.location1.z, req.body.volume]     
      );
    }
    else if (req.body.media_type == '2d') {
      await client.query(
      `INSERT INTO user_2dmedia (id_2d, user_id, locationx_tr, locationy_tr, locationz_tr, locationx_bl,locationy_bl, locationz_bl,
      locationx_br, locationy_br, locationz_br)
      VALUES
      ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
      [req.body.media_id, req.body.user_id, req.body.location1.x,
      req.body.location1.y, req.body.location1.z, req.body.location2.x,
      req.body.location2.y, req.body.location2.z, req.body.location3.x,
      req.body.location3.y, req.body.location3.z]
    );
    }
    else if (req.body.media_type == '3d') {
      await client.query(
      `INSERT INTO user_3dmedia (id_3d, user_id, locationx_ftr, locationy_ftr, locationz_ftr, locationx_fbl,locationy_fbl, locationz_fbl,
      locationx_bbr, locationy_bbr, locationz_bbr)
      VALUES
      ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`,
      [req.body.media_id, req.body.user_id, req.body.location1.x,
      req.body.location1.y, req.body.location1.z, req.body.location2.x,
      req.body.location2.y, req.body.location2.z, req.body.location3.x,
      req.body.location3.y, req.body.location3.z]
    );
    }
    else {
      //returns client error if invalid  media
      return res.status(403).json({
        message: 'invalid media type'
      })
    }
      
    return res.status(200).json({
        message: 'ok!'
    });
};

// Modify media location in virtual world
const updateMedia = async (req: Request, res: Response, next: NextFunction) => {
    const client = await getDB();
    if (req.body.media_type =='audio'){
      await client.query(
        `UPDATE user_audiomedia
        SET locationx = $1,
        locationy = $2,
        locationz = $3,
        volume = $4
        WHERE id= $5`,
        [req.body.location1.x, req.body.location1.y, req.body.location1.z,
          req.body.volume, req.body.object_id]
      )  
    }
    else if (req.body.media_type =='2d'){
      await client.query(
        `UPDATE user_2dmedia
        SET locationx_tr = $1,
        locationy_tr = $2,
        locationz_tr = $3,
        locationx_bl = $4,
        locationy_bl = $5,
        locationz_bl = $6,
        locationx_br = $7,
        locationy_br = $8,
        locationz_br = $9
        WHERE id= $10`,
        [req.body.location1.x,req.body.location1.y, req.body.location1.z, req.body.location2.x,
        req.body.location2.y, req.body.location2.z, req.body.location3.x,
        req.body.location3.y, req.body.location3.z, req.body.object_id]
      )  
    }
    else if (req.body.media_type =='3d'){
      await client.query(
        `UPDATE user_3dmedia
        SET locationx_ftr = $1,
        locationy_ftr = $2,
        locationz_ftr = $3,
        locationx_fbl = $4,
        locationy_fbl = $5,
        locationz_fbl = $6,
        locationx_bbr = $7,
        locationy_bbr = $8,
        locationz_bbr = $9
        WHERE id= $10`,
        [req.body.location1.x,req.body.location1.y, req.body.location1.z, req.body.location2.x,
        req.body.location2.y, req.body.location2.z, req.body.location3.x,
        req.body.location3.y, req.body.location3.z, req.body.object_id]
      )  
    }
    else {
      //returns client error if invalid  media
      return res.status(403).json({
        message: 'invalid media type'
      })
    }

    return res.status(200).json({
        message: 'ok!'
    });
};



/* 
ASSUMPTIONS:
1. 2D media is in the shape of a rectangle
2. 3D media is contained in a box
3. audio is omnidirectional, from a single point 

FUTURE DIRECTIONS: 
1. delete media function
2. ownership (e.g. right now, any user can move any object) 
3. security considerations (access control of functions. admin role, moderator, etc)
4. batch updates (e.g. move multiple items at once within a certain area)
5. consider object collisions (e.g. do not update if colliding)
6. catch database errors (e.g. if an inventory media_id does not exist, return a 403)

*/
//connects to the db. ideally would have a database pool instead of creating a new connection on every request
const getDB = async () => 
  {
    const client = new Client({database: "niftyisland"});
    await client.connect();
    return client;
  };
  
  
const myrouter = Router();
myrouter.get('/media', getAllMedia);
myrouter.get('/inventory', getAllInventory);
myrouter.post('/media', addMedia);
myrouter.put('/media', updateMedia);
//creates express server
let router: Express = express();
//enable clients to send json 
router.use(express.json());
router.use('/', myrouter);

//start the server
const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 6060;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));


