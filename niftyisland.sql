BEGIN;

CREATE TABLE ni_user (
	user_id serial PRIMARY KEY
);

CREATE TABLE audio_media (
	audio_id serial PRIMARY KEY,
	url VARCHAR UNIQUE NOT NULL
);

CREATE TABLE user_audiomedia (
	id serial PRIMARY KEY,
	user_id INT NOT NULL REFERENCES ni_user (user_id),
	audio_id INT NOT NULL REFERENCES audio_media (audio_id),
	locationx INT NOT NULL,
	locationy INT NOT NULL,
	locationz INT NOT NULL,
	volume INT NOT NULL CHECK (volume >= -100 AND volume <= 100)
);

CREATE TABLE media_2d (
	id_2d serial PRIMARY KEY,
	url VARCHAR UNIQUE NOT NULL
);


CREATE TABLE user_2dmedia (
	id serial PRIMARY KEY,
	user_id INT NOT NULL REFERENCES ni_user (user_id),
	id_2d INT NOT NULL REFERENCES media_2d (id_2d),
	--could have used a new type but using this instead for simplicity
	--tr is 'top right', bl is 'bottom left', br is 'bottom right'
	locationx_tr INT NOT NULL,
	locationy_tr INT NOT NULL,
	locationz_tr INT NOT NULL, 
	locationx_bl INT NOT NULL,
	locationy_bl INT NOT NULL,
	locationz_bl INT NOT NULL, 
	locationx_br INT NOT NULL,
	locationy_br INT NOT NULL,
	locationz_br INT NOT NULL 
);

CREATE TABLE media_3d (
	id_3d serial PRIMARY KEY,
	url VARCHAR UNIQUE NOT NULL
);

CREATE TABLE user_3dmedia (
	id serial PRIMARY KEY,
	user_id INT NOT NULL REFERENCES ni_user (user_id),
	id_3d INT NOT NULL REFERENCES media_3d (id_3d),
	--ftr is 'front top right', fbl is 'front bottom left', etc
	locationx_ftr INT NOT NULL,
	locationy_ftr INT NOT NULL,
	locationz_ftr INT NOT NULL, 
	locationx_fbl INT NOT NULL,
	locationy_fbl INT NOT NULL,
	locationz_fbl INT NOT NULL, 
	locationx_bbr INT NOT NULL,
	locationy_bbr INT NOT NULL,
	locationz_bbr INT NOT NULL 
);

--test data

INSERT INTO ni_user (user_id) VALUES (1234);
INSERT INTO ni_user (user_id) VALUES (1235);
INSERT INTO ni_user (user_id) VALUES (1236);
INSERT INTO ni_user (user_id) VALUES (1237);
INSERT INTO audio_media (audio_id, url) VALUES (1111, 'http://thisisaudio.com/audio.wav');
INSERT INTO user_audiomedia (user_id, audio_id, locationx,  locationy, locationz, volume) 
VALUES (1234,1111, 0,5,100,20);
-- duplicate audio file for user 
INSERT INTO user_audiomedia (user_id, audio_id, locationx,  locationy, locationz, volume) 
VALUES (1234,1111, 2,5,100,22);
INSERT INTO media_2d (id_2d, url) VALUES (2222, 'http://thisis2dmedia.com/ferret.jpeg');
INSERT INTO user_2dmedia (user_id, id_2d, locationx_tr, locationy_tr, locationz_tr,
locationx_bl, locationy_bl, locationz_bl, locationx_br, locationy_br, locationz_br)
VALUES
(1235, 2222, 1,5,0, -12, -22, -1, 0, -22, -5);
INSERT INTO media_3d (id_3d, url) VALUES (3333, 'http://thisis3dmedia.com/doggie.obj');
INSERT INTO user_3dmedia (user_id, id_3d, locationx_ftr, locationy_ftr, locationz_ftr,
locationx_fbl, locationy_fbl, locationz_fbl, locationx_bbr, locationy_bbr, locationz_bbr)
VALUES
(1236, 3333, 20,200,50, 2,10,5, 10, -20, -55);


COMMIT;
