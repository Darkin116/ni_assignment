#show everything executed and fail if command fails
set -xe

#test getAllInventory
curl http://localhost:6060/inventory

#test getAllMedia
curl http://localhost:6060/media

#duplicate audio
curl http://localhost:6060/media -d '{"media_type": "audio", "media_id":1111,"user_id":1234,"location1":{"x":25,"y":25,"z":25},"volume":25}' -H 'Content-Type: application/json' -H 'Accept: application/json'

#duplicate 2d
curl http://localhost:6060/media -d '{"media_type": "2d", "media_id":2222,"user_id":1235,"location1":{"x":25,"y":25,"z":25}, "location2":{"x":25,"y":25,"z":25}, "location3":{"x":25,"y":25,"z":25}}' -H 'Content-Type: application/json' -H 'Accept: application/json'

#duplicate 3d
curl http://localhost:6060/media -d '{"media_type": "3d", "media_id":3333,"user_id":1235,"location1":{"x":25,"y":25,"z":25}, "location2":{"x":25,"y":25,"z":25}, "location3":{"x":25,"y":25,"z":25}}' -H 'Content-Type: application/json' -H 'Accept: application/json'

#modify audio
curl http://localhost:6060/media -d '{"media_type": "audio", "object_id":1, "location1":{"x":30,"y":30,"z":30},"volume":35}' -H 'Content-Type: application/json' -H 'Accept: application/json' -X PUT

#modify 2d
curl http://localhost:6060/media -d '{"media_type": "2d", "object_id":2,"user_id":1235,"location1":{"x":45,"y":45,"z":45}, "location2":{"x":25,"y":25,"z":25}, "location3":{"x":25,"y":25,"z":25}}' -H 'Content-Type: application/json' -H 'Accept: application/json' -X PUT

#modify 3d
curl http://localhost:6060/media -d '{"media_type": "3d", "object_id":3,"user_id":1235,"location1":{"x":25,"y":25,"z":25}, "location2":{"x":25,"y":25,"z":25}, "location3":{"x":25,"y":25,"z":25}}' -H 'Content-Type: application/json' -H 'Accept: application/json' -X PUT


